// Copyright (c) 2015, One Off Code LLC, All Rights Reserveeed
package bosun

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/rcrowley/go-metrics"
)

type BosunConfig struct {
	//Addr          *net.TCPAddr     // Network address to connect to
	Addr          string
	Registry      metrics.Registry // Registry to be exported
	FlushInterval time.Duration    // Flush interval
	DurationUnit  time.Duration    // Time conversion unit for durations
	Prefix        string           // Prefix to be prepended to metric names
	Percentiles   []float64        // Percentiles to export from timers and histograms
	Tags          TagSet
}

//func Bosun(r metrics.Registry, d time.Duration, prefix string, addr *net.TCPAddr) {
func Bosun(r metrics.Registry, d time.Duration, prefix string, addr string, tags TagSet) {
	BosunWithConfig(BosunConfig{
		Addr:          addr,
		Registry:      r,
		FlushInterval: d,
		DurationUnit:  time.Nanosecond,
		Prefix:        prefix,
		Percentiles:   []float64{0.5, 0.75, 0.95, 0.99, 0.999},
		Tags:          tags,
	})
}

type TagSet map[string]string

type DataPoint struct {
	Metric    string      `json:"metric"`
	Timestamp int64       `json:"timestamp"`
	Value     interface{} `json:"value"`
	Tags      TagSet      `json:"tags"`
}

func BosunWithConfig(c BosunConfig) {
	for _ = range time.Tick(c.FlushInterval) {
		if err := bosun(&c); nil != err {
			log.Println(err)
		}
	}
}

func BosunOnce(c BosunConfig) error {
	return bosun(&c)
}

func makeDataPoint(prefix, name, attr string, value interface{}, timestamp int64, tags TagSet) DataPoint {
	return DataPoint{Metric: prefix + "." + name + "." + attr,
		Value:     value,
		Timestamp: timestamp,
		Tags:      tags}
}

func bosun(c *BosunConfig) error {
	bosunurl := c.Addr + "/api/put?summary"
	//url := c.Addr
	now := time.Now().Unix()
	du := float64(c.DurationUnit)
	c.Registry.Each(func(name string, i interface{}) {
		d := make([]DataPoint, 0, 0)
		switch metric := i.(type) {
		case metrics.Counter:
			d = append(d, makeDataPoint(c.Prefix, name, "count", metric.Count(), now, c.Tags))
		case metrics.Gauge:
			d = append(d, makeDataPoint(c.Prefix, name, "value", metric.Value(), now, c.Tags))
		case metrics.GaugeFloat64:
			d = append(d, makeDataPoint(c.Prefix, name, "value", metric.Value(), now, c.Tags))
		case metrics.Histogram:
			h := metric.Snapshot()
			ps := h.Percentiles(c.Percentiles)
			d = append(d, makeDataPoint(c.Prefix, name, "count", h.Count(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "min", h.Min(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "max", h.Max(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "mean", h.Mean(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "std-dev", h.StdDev(), now, c.Tags))
			for psIdx, psKey := range c.Percentiles {
				key := strings.Replace(strconv.FormatFloat(psKey*100.0, 'f', -1, 64), ".", "", 1)
				d = append(d, makeDataPoint(c.Prefix, name, key+"-percentile", ps[psIdx], now, c.Tags))
			}
		case metrics.Meter:
			m := metric.Snapshot()
			d = append(d, makeDataPoint(c.Prefix, name, "count", m.Count(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "one-minute", m.Rate1(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "five-minute", m.Rate5(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "fifteen-minute", m.Rate15(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "mean", m.RateMean(), now, c.Tags))
		case metrics.Timer:
			t := metric.Snapshot()
			ps := t.Percentiles(c.Percentiles)
			d = append(d, makeDataPoint(c.Prefix, name, "count", t.Count(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "min", t.Min()/int64(du), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "max", t.Max()/int64(du), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "mean", t.Mean()/du, now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "std-dev", t.StdDev()/du, now, c.Tags))
			for psIdx, psKey := range c.Percentiles {
				key := strings.Replace(strconv.FormatFloat(psKey*100.0, 'f', -1, 64), ".", "", 1)
				d = append(d, makeDataPoint(c.Prefix, name, key+"-percentile", ps[psIdx], now, c.Tags))
			}
			d = append(d, makeDataPoint(c.Prefix, name, "one-minute", t.Rate1(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "five-minute", t.Rate5(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "fifteen-minute", t.Rate15(), now, c.Tags))
			d = append(d, makeDataPoint(c.Prefix, name, "mean-rate", t.RateMean(), now, c.Tags))
		}
		buf, err := json.Marshal(d)
		if err != nil {
			log.Printf("ERROR: %#v", err)
			return
		}
		resp, err := http.Post(bosunurl, "application/json", bytes.NewBuffer(buf))
		if err != nil {
			urlError, _ := err.(*url.Error)
			log.Printf("ERROR: %#v", urlError.Err)
			return
		}
		defer resp.Body.Close()
		if resp.StatusCode < 200 || resp.StatusCode >= 300 {
			log.Printf("ERROR: incorrect StatusCode, %#v, expoecting 2XX", resp)
		}
	})
	return nil
}
