# go-metrics-bosun #

This is a reporter for the [go-metrics](https://github.com/rcrowley/go-metrics)
library which will post the metrics to Bosun. There is an OpenTSDB package
that will eventually fork, however, the current state of Bosun suggests
that the client should send JSON messages and not Graphite formatted metrics.

### Usage

```go
import "github.com/cyberdelia/go-metrics-bosun"


go bosun.Bosun(metrics.DefaultRegistry,
  1*time.Second, "some.prefix", "http://mybosun.server:8070", map[string]string{"host":"myhost"})
```

Notice that I added a tagset to the bosun config. According to the OpenTSDB `/api/put` [documentation](http://opentsdb.net/docs/build/html/api_http/put.html) 
"At least one pair must be supplied." From the Bosun GUI it makes the most sense to include the "host" tag.

# TODO #

- needs metadata `/api/metadata/put`


# LICENSE #

BSD (2 clause).